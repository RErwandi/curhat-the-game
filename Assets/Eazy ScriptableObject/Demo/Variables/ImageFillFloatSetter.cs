﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace EazyScriptableObject
{
    public class ImageFillFloatSetter : MonoBehaviour
    {
        [SerializeField] private FloatVariable currentHp = default;
        [SerializeField] private FloatVariable maxHp = default;
        [SerializeField] private Image image = default;

        private void OnEnable()
        {
            currentHp.AddListener(Repaint);
            maxHp.AddListener(Repaint);
        }

        private void OnDisable()
        {
            currentHp.RemoveListener(Repaint);
            maxHp.RemoveListener(Repaint);
        }

        private void Start()
        {
            Repaint();
        }

        private void Repaint()
        {
            image.fillAmount = Mathf.Clamp01(currentHp.Value / maxHp.Value);
        }
    }
}
