﻿using System.Collections;
using System.Collections.Generic;
using EazyScriptableObject;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHP : MonoBehaviour
{
    public FloatReference hp;
    public Text text;
    
    void Update()
    {
        text.text = "My current HP is " + hp.Value;
    }
}
