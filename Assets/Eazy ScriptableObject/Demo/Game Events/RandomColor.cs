﻿using System.Collections;
using System.Collections.Generic;
using EazyScriptableObject;
using UnityEngine;

public class RandomColor : MonoBehaviour
{
    public void RandomizeColor()
    {
        var r = Random.Range(0, 3);
        switch (r)
        {
            case 0:
                GetComponent<Renderer>().material.color = Color.red;
                break;
            case 1:
                GetComponent<Renderer>().material.color = Color.green;
                break;
            case 2:
                GetComponent<Renderer>().material.color = Color.blue;
                break;
            default:
                GetComponent<Renderer>().material.color = Color.gray;
                break;
        }
        
    }
}
