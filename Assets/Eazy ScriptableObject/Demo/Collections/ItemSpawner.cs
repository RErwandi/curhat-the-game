﻿using System.Collections;
using System.Collections.Generic;
using EazyScriptableObject;
using UnityEngine;

public class ItemSpawner : MonoBehaviour
{
    [SerializeField] private GameObjectCollection collection = default;
    
    public void SpawnItem()
    {
        var x = Random.Range(-5f, 5f);
        var y = Random.Range(-5f, 5f);

        Instantiate(collection[collection.Count - 1], new Vector3(x, y, 0f), Quaternion.identity);
    }
}
