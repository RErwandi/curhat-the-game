﻿using UnityEngine;

namespace EazyScriptableObject
{
    [System.Serializable]
    [CreateAssetMenu(
        fileName = "DoubleGameEvent.asset",
        menuName = EazySOUtility.ADD_COMPONENT_MENU_ROOT + EazySOUtility.GAME_EVENT_SUBMENU + "double",
        order = EazySOUtility.ASSET_MENU_ORDER_EVENTS)]
    public sealed class DoubleGameEvent : GameEventBase<double>
    {
    } 
}