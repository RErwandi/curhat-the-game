using UnityEngine;

namespace EazyScriptableObject
{
    [System.Serializable]
    [CreateAssetMenu(
        fileName = "Vector2GameEvent.asset",
        menuName = EazySOUtility.ADD_COMPONENT_MENU_ROOT + EazySOUtility.GAME_EVENT_SUBMENU + "Structs/Vector2",
        order = EazySOUtility.ASSET_MENU_ORDER_EVENTS)]
    public sealed class Vector2GameEvent : GameEventBase<Vector2>
    {
    } 
}