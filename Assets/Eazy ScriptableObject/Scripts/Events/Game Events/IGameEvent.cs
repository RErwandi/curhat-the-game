﻿using System;

namespace EazyScriptableObject
{
    public interface IGameEvent<T>
    {
        void Raise(T value);
        void AddListener(Action<T> listener);
        void RemoveListener(Action<T> listener);
    }

    public interface IGameEvent
    {
        void Raise();
        void AddListener(Action listener);
        void RemoveListener(Action listener);
    }
}