﻿using UnityEngine;

namespace EazyScriptableObject
{
    [System.Serializable]
    [CreateAssetMenu(
        fileName = "FloatGameEvent.asset",
        menuName = EazySOUtility.ADD_COMPONENT_MENU_ROOT + EazySOUtility.GAME_EVENT_SUBMENU + "float",
        order = EazySOUtility.ASSET_MENU_ORDER_EVENTS)]
    public sealed class FloatGameEvent : GameEventBase<float>
    {
    } 
}