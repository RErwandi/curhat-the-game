using UnityEngine;

namespace EazyScriptableObject
{
    [System.Serializable]
    [CreateAssetMenu(
        fileName = "GameObjectGameEvent.asset",
        menuName = EazySOUtility.ADD_COMPONENT_MENU_ROOT + EazySOUtility.GAME_EVENT_SUBMENU + "GameObject",
        order = EazySOUtility.ASSET_MENU_ORDER_EVENTS)]
    public sealed class GameObjectGameEvent : GameEventBase<GameObject>
    {
    } 
}