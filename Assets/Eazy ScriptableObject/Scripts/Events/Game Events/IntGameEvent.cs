﻿using UnityEngine;

namespace EazyScriptableObject
{
    [System.Serializable]
    [CreateAssetMenu(
        fileName = "IntGameEvent.asset",
        menuName = EazySOUtility.ADD_COMPONENT_MENU_ROOT + EazySOUtility.GAME_EVENT_SUBMENU + "int",
        order = EazySOUtility.ASSET_MENU_ORDER_EVENTS)]
    public sealed class IntGameEvent : GameEventBase<int>
    {
    } 
}