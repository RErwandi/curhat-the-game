using UnityEngine;

namespace EazyScriptableObject
{
    [System.Serializable]
    [CreateAssetMenu(
        fileName = "QuaternionGameEvent.asset",
        menuName = EazySOUtility.ADD_COMPONENT_MENU_ROOT + EazySOUtility.GAME_EVENT_SUBMENU + "Structs/Quaternion",
        order = EazySOUtility.ASSET_MENU_ORDER_EVENTS)]
    public sealed class QuaternionGameEvent : GameEventBase<Quaternion>
    {
    } 
}