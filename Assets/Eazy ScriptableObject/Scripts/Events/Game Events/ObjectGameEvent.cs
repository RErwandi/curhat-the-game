﻿using UnityEngine;

namespace EazyScriptableObject
{
    [System.Serializable]
    [CreateAssetMenu(
        fileName = "ObjectGameEvent.asset",
        menuName =EazySOUtility.ADD_COMPONENT_MENU_ROOT + EazySOUtility.GAME_EVENT_SUBMENU + "Object",
        order = EazySOUtility.ASSET_MENU_ORDER_EVENTS)]
    public class ObjectGameEvent : GameEventBase<Object>
    {
    } 
}