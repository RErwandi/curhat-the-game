﻿using UnityEngine;

namespace EazyScriptableObject
{
    [CreateAssetMenu(
        fileName = "GameEvent.asset",
        menuName = EazySOUtility.ADD_COMPONENT_MENU_ROOT + EazySOUtility.GAME_EVENT_SUBMENU + "Game Event",
        order = EazySOUtility.ASSET_MENU_ORDER_EVENTS)]
    public sealed class GameEvent : GameEventBase
    {
    } 
}