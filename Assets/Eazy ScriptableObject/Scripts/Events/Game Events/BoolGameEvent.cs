﻿using UnityEngine;

namespace EazyScriptableObject
{
    [System.Serializable]
    [CreateAssetMenu(
        fileName = "BoolGameEvent.asset",
        menuName = EazySOUtility.ADD_COMPONENT_MENU_ROOT + EazySOUtility.GAME_EVENT_SUBMENU + "bool",
        order = EazySOUtility.ASSET_MENU_ORDER_EVENTS)]
    public sealed class BoolGameEvent : GameEventBase<bool>
    {
    } 
}