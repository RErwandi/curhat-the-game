﻿using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace EazyScriptableObject
{
    public abstract class GameEventBase<T> : GameEventBase, IGameEvent<T>
    {
        [LabelText("GameObject listening to this event")]
        [ShowInInspector][ReadOnly]
        private List<Action<T>> typedListeners = new List<Action<T>>();
        
        [ShowIf("$ShowDebuggingOptions")]
        [BoxGroup("Debugging Options")]
        [SerializeField] private T debugValue = default(T);
        
        [ShowIf("$ShowDebuggingOptions")]
        [BoxGroup("Debugging Options")]
        [DisableInEditorMode]
        [Button(ButtonSizes.Large)]
        private void DebugRaise()
        {
            Raise(debugValue);
        }

        public void Raise(T val)
        {
            if (EazySOSettings.Instance.showDebugWhenEventRaised)
            {
                Debug.Log($"{name} Game Event Raised"); 
            }
            
            foreach (var typedListener in typedListeners)
            {
                typedListener.Invoke(val);
            }
            
            Raise();
        }

        public virtual void AddListener(Action<T> listener)
        {
            if (listener == null) return;
            if (!typedListeners.Contains(listener))
            {
                typedListeners.Add(listener);
            }
        }

        public virtual void RemoveListener(Action<T> listener)
        {
            if (listener == null) return;
            if (typedListeners.Contains(listener))
            {
                typedListeners.Remove(listener);
            }
        }

        protected virtual bool ShowDebuggingOptions()
        {
            return true;
        }
    }

    public abstract class GameEventBase : ScriptableObject, IGameEvent
    {
        [SerializeField][ReadOnly]
        protected List<Action> listeners = new List<Action>();
        
        [BoxGroup("Debugging Options")]
        public string description;
        
        [BoxGroup("Debugging Options")]
        [DisableInEditorMode]
        [Button(ButtonSizes.Large)]
        private void DebugRaise()
        {
            Raise();
        }

        public void Raise()
        {
            if (EazySOSettings.Instance.showDebugWhenEventRaised)
            {
                Debug.Log($"{name} Game Event Raised"); 
            }
            
            #if dUI_MANAGER
                Doozy.Engine.GameEventMessage.SendEvent(name);
            #endif
            
            foreach (var listener in listeners)
            {
                listener.Invoke();
            }
        }

        public virtual void AddListener(Action listener)
        {
            if (listener == null) return;
            if (!listeners.Contains(listener))
            {
                listeners.Add(listener);
            }
        }

        public virtual void RemoveListener(Action listener)
        {
            if (listener == null) return;
            if (listeners.Contains(listener))
            {
                listeners.Remove(listener);
            }
        }
    }
}
