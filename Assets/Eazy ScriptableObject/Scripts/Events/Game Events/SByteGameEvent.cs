﻿using UnityEngine;

namespace EazyScriptableObject
{
    [System.Serializable]
    [CreateAssetMenu(
        fileName = "SignedByteGameEvent.asset",
        menuName = EazySOUtility.ADD_COMPONENT_MENU_ROOT + EazySOUtility.GAME_EVENT_SUBMENU + "sbyte",
        order = EazySOUtility.ASSET_MENU_ORDER_EVENTS)]
    public sealed class SByteGameEvent : GameEventBase<sbyte>
    {
    } 
}