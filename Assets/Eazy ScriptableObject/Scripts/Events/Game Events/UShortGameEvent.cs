﻿using UnityEngine;

namespace EazyScriptableObject
{
    [System.Serializable]
    [CreateAssetMenu(
        fileName = "UnsignedShortGameEvent.asset",
        menuName = EazySOUtility.ADD_COMPONENT_MENU_ROOT + EazySOUtility.GAME_EVENT_SUBMENU + "ushort",
        order = EazySOUtility.ASSET_MENU_ORDER_EVENTS)]
    public sealed class UShortGameEvent : GameEventBase<ushort>
    {
    } 
}