﻿using UnityEngine;

namespace EazyScriptableObject
{
    [System.Serializable]
    [CreateAssetMenu(
        fileName = "ByteGameEvent.asset",
        menuName = EazySOUtility.ADD_COMPONENT_MENU_ROOT + EazySOUtility.GAME_EVENT_SUBMENU + "byte",
        order = EazySOUtility.ASSET_MENU_ORDER_EVENTS)]
    public sealed class ByteGameEvent : GameEventBase<byte>
    {
    } 
}