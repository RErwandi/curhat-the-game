using UnityEngine;

namespace EazyScriptableObject
{
    [System.Serializable]
    [CreateAssetMenu(
        fileName = "Vector3GameEvent.asset",
        menuName = EazySOUtility.ADD_COMPONENT_MENU_ROOT + EazySOUtility.GAME_EVENT_SUBMENU + "Structs/Vector3",
        order = EazySOUtility.ASSET_MENU_ORDER_EVENTS)]
    public sealed class Vector3GameEvent : GameEventBase<Vector3>
    {
    } 
}