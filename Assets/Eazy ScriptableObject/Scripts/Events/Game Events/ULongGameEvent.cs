﻿using UnityEngine;

namespace EazyScriptableObject
{
    [System.Serializable]
    [CreateAssetMenu(
        fileName = "UnsignedLongGameEvent.asset",
        menuName = EazySOUtility.ADD_COMPONENT_MENU_ROOT + EazySOUtility.GAME_EVENT_SUBMENU + "ulong",
        order = EazySOUtility.ASSET_MENU_ORDER_EVENTS)]
    public sealed class ULongGameEvent : GameEventBase<ulong>
    {
    } 
}