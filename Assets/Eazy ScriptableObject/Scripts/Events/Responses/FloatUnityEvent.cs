﻿using UnityEngine.Events;

namespace EazyScriptableObject
{
    [System.Serializable]
    public sealed class FloatUnityEvent : UnityEvent<float>
    {
    } 
}