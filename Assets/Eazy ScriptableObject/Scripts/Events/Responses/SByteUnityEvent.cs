﻿using UnityEngine.Events;

namespace EazyScriptableObject
{
    [System.Serializable]
    public sealed class SByteUnityEvent : UnityEvent<sbyte>
    {
    } 
}