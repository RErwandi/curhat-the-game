﻿using UnityEngine.Events;

namespace EazyScriptableObject
{
    [System.Serializable]
    public sealed class CharUnityEvent : UnityEvent<char>
    {
    } 
}