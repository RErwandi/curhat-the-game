﻿using UnityEngine.Events;

namespace EazyScriptableObject
{
    [System.Serializable]
    public sealed class BoolUnityEvent : UnityEvent<bool>
    {
    } 
}