﻿using UnityEngine.Events;

namespace EazyScriptableObject
{
    [System.Serializable]
    public sealed class IntUnityEvent : UnityEvent<int>
    {
    } 
}