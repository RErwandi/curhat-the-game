﻿using UnityEngine.Events;

namespace EazyScriptableObject
{
    [System.Serializable]
    public sealed class ShortUnityEvent : UnityEvent<short>
    {
    } 
}