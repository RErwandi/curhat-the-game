﻿using UnityEngine.Events;

namespace EazyScriptableObject
{
    [System.Serializable]
    public sealed class ByteUnityEvent : UnityEvent<byte>
    {
    } 
}