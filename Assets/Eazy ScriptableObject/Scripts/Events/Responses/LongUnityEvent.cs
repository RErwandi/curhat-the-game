﻿using UnityEngine.Events;

namespace EazyScriptableObject
{
    [System.Serializable]
    public sealed class LongUnityEvent : UnityEvent<long>
    {
    } 
}