using UnityEngine;
using UnityEngine.Events;

namespace EazyScriptableObject
{
    [System.Serializable]
    public sealed class QuaternionUnityEvent : UnityEvent<Quaternion>
    {
    } 
}