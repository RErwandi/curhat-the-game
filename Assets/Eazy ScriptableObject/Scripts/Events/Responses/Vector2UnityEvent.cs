using UnityEngine;
using UnityEngine.Events;

namespace EazyScriptableObject
{
    [System.Serializable]
    public sealed class Vector2UnityEvent : UnityEvent<Vector2>
    {
    } 
}