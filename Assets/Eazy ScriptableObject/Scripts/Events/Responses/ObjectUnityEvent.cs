﻿using UnityEngine;
using UnityEngine.Events;

namespace EazyScriptableObject
{
    [System.Serializable]
    public class ObjectUnityEvent : UnityEvent<Object>
    {
    } 
}