﻿using UnityEngine.Events;

namespace EazyScriptableObject
{
    [System.Serializable]
    public sealed class UShortUnityEvent : UnityEvent<ushort>
    {
    } 
}