﻿using System.Collections;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

namespace EazyScriptableObject
{
    public abstract class BaseGameEventListener<TType, TEvent, TResponse> : MonoBehaviour
        where TEvent : GameEventBase<TType>
        where TResponse : UnityEvent<TType>
    {
        [ValueDropdown("GetAllCompatibleScriptableObjects", AppendNextDrawer = true)]
        [InfoBox("$GetDescription")]
        public TEvent gameEvent;
        public TResponse response;
        
        private void OnEnable()
        {
            if (gameEvent != null)
            {
                gameEvent.AddListener(OnEventRaised);
            }
        }

        private void OnDisable()
        {
            if (gameEvent != null)
            {
                gameEvent.RemoveListener(OnEventRaised);
            }
        }

        private void OnEventRaised(TType value)
        {
            response.Invoke(value);
        }
        
#if UNITY_EDITOR
        private static IEnumerable GetAllCompatibleScriptableObjects()
        {
            var className = typeof(TEvent).ToString();
            return UnityEditor.AssetDatabase.FindAssets($"t:{className}")
                .Select(x => UnityEditor.AssetDatabase.GUIDToAssetPath(x))
                .Select(x => new ValueDropdownItem(x, UnityEditor.AssetDatabase.LoadAssetAtPath<TEvent>(x)));
        }
#endif
        
        private string GetDescription()
        {
            return gameEvent != null ? gameEvent.description : "No game event attached";
        }
    }
    
    public abstract class BaseGameEventListener<TEvent, TResponse> : MonoBehaviour
        where TEvent : GameEventBase
        where TResponse : UnityEvent
    {
        [ValueDropdown("GetAllCompatibleScriptableObjects", AppendNextDrawer = true)]
        [InfoBox("$GetDescription")]
        public TEvent gameEvent = default;
        public TResponse response = default;

        private void OnEnable()
        {
            gameEvent.AddListener(OnEventRaised);
        }

        private void OnDisable()
        {
            gameEvent.RemoveListener(OnEventRaised);
        }

        private void OnEventRaised()
        {
            response.Invoke();
        }
        
#if UNITY_EDITOR
        private static IEnumerable GetAllCompatibleScriptableObjects()
        {
            return UnityEditor.AssetDatabase.FindAssets("t:GameEvent")
                .Select(x => UnityEditor.AssetDatabase.GUIDToAssetPath(x))
                .Select(x => new ValueDropdownItem(x, UnityEditor.AssetDatabase.LoadAssetAtPath<TEvent>(x)));
        }
#endif

        private string GetDescription()
        {
            return gameEvent != null ? gameEvent.description : "No game event attached";
        }
    }
}