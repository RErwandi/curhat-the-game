﻿using UnityEngine;

namespace EazyScriptableObject
{
    [AddComponentMenu(EazySOUtility.EVENT_LISTENER_SUBMENU + "float Event Listener")]
    public sealed class FloatGameEventListener : BaseGameEventListener<float, FloatGameEvent, FloatUnityEvent>
    {
    }
}