﻿using UnityEngine;

namespace EazyScriptableObject
{
    [AddComponentMenu(EazySOUtility.EVENT_LISTENER_SUBMENU + "ushort Event Listener")]
    public sealed class UShortGameEventListener : BaseGameEventListener<ushort, UShortGameEvent, UShortUnityEvent>
    {
    }
}