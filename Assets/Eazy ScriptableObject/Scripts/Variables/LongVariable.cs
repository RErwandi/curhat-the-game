﻿using UnityEngine;

namespace EazyScriptableObject
{
    [CreateAssetMenu(
        fileName = "LongVariable.asset",
        menuName = EazySOUtility.ADD_COMPONENT_MENU_ROOT + EazySOUtility.VARIABLE_SUBMENU + "long",
        order = EazySOUtility.ASSET_MENU_ORDER_VARIABLES)]
    public class LongVariable : BaseVariable<long>
    {
    } 
}