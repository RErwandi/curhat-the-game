﻿using UnityEngine;

namespace EazyScriptableObject
{
    [CreateAssetMenu(
        fileName = "IntVariable.asset",
        menuName = EazySOUtility.ADD_COMPONENT_MENU_ROOT + EazySOUtility.VARIABLE_SUBMENU + "int",
        order = EazySOUtility.ASSET_MENU_ORDER_VARIABLES)]
    public class IntVariable : BaseVariable<int>
    {
    } 
}