using UnityEngine;

namespace EazyScriptableObject
{
    [CreateAssetMenu(
        fileName = "Vector3Variable.asset",
        menuName =EazySOUtility.ADD_COMPONENT_MENU_ROOT + EazySOUtility.VARIABLE_SUBMENU + "Structs/Vector3",
        order = EazySOUtility.ASSET_MENU_ORDER_VARIABLES)]
    public sealed class Vector3Variable : BaseVariable<Vector3>
    {
    } 
}