﻿using UnityEngine;

namespace EazyScriptableObject
{
    [CreateAssetMenu(
        fileName = "CharVariable.asset",
        menuName = EazySOUtility.ADD_COMPONENT_MENU_ROOT + EazySOUtility.VARIABLE_SUBMENU + "char",
        order = EazySOUtility.ASSET_MENU_ORDER_VARIABLES)]
    public sealed class CharVariable : BaseVariable<char>
    {
    } 
}