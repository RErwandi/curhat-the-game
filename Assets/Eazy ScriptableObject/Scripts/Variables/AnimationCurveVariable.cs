using UnityEngine;

namespace EazyScriptableObject
{
	[CreateAssetMenu(
	    fileName = "AnimationCurveVariable.asset",
	    menuName = EazySOUtility.ADD_COMPONENT_MENU_ROOT + EazySOUtility.VARIABLE_SUBMENU + "AnimationCurve",
	    order = EazySOUtility.ASSET_MENU_ORDER_VARIABLES)]
	public class AnimationCurveVariable : BaseVariable<AnimationCurve>
	{
	}
}