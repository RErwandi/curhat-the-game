using UnityEngine;

namespace EazyScriptableObject
{
	[CreateAssetMenu(
	    fileName = "Color32Variable.asset",
	    menuName = EazySOUtility.ADD_COMPONENT_MENU_ROOT + EazySOUtility.VARIABLE_SUBMENU + "Structs/Color32",
	    order = EazySOUtility.ASSET_MENU_ORDER_VARIABLES)]
	public class Color32Variable : BaseVariable<Color32>
	{
	}
}