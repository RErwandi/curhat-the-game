﻿using UnityEngine;

namespace EazyScriptableObject
{
    [CreateAssetMenu(
        fileName = "SByteVariable.asset",
        menuName = EazySOUtility.ADD_COMPONENT_MENU_ROOT + EazySOUtility.VARIABLE_SUBMENU + "sbyte",
        order = EazySOUtility.ASSET_MENU_ORDER_VARIABLES)]
    public class SByteVariable : BaseVariable<sbyte>
    {
    } 
}