﻿using UnityEngine;

namespace EazyScriptableObject
{
    [CreateAssetMenu(
        fileName = "UnsignedIntVariable.asset",
        menuName = EazySOUtility.ADD_COMPONENT_MENU_ROOT + EazySOUtility.VARIABLE_SUBMENU + "uint",
        order = EazySOUtility.ASSET_MENU_ORDER_VARIABLES)]
    public class UIntVariable : BaseVariable<uint>
    {
    } 
}