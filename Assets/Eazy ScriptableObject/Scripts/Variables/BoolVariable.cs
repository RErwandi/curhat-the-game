﻿using UnityEngine;

namespace EazyScriptableObject
{
    [CreateAssetMenu(
        fileName = "BoolVariable.asset",
        menuName = EazySOUtility.ADD_COMPONENT_MENU_ROOT + EazySOUtility.VARIABLE_SUBMENU + "bool",
        order = EazySOUtility.ASSET_MENU_ORDER_VARIABLES)]
    public sealed class BoolVariable : BaseVariable<bool>
    {
    } 
}