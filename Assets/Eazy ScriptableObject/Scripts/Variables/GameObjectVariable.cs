using UnityEngine;

namespace EazyScriptableObject
{
    [CreateAssetMenu(
        fileName = "GameObjectVariable.asset",
        menuName = EazySOUtility.ADD_COMPONENT_MENU_ROOT + EazySOUtility.VARIABLE_SUBMENU + "GameObject",
        order = EazySOUtility.ASSET_MENU_ORDER_VARIABLES)]
    public sealed class GameObjectVariable : BaseVariable<GameObject>
    {
    } 
}