﻿using UnityEngine;

namespace EazyScriptableObject
{
    [CreateAssetMenu(
        fileName = "StringVariable.asset",
        menuName = EazySOUtility.ADD_COMPONENT_MENU_ROOT + EazySOUtility.VARIABLE_SUBMENU + "string",
        order = EazySOUtility.ASSET_MENU_ORDER_VARIABLES)]
    public sealed class StringVariable : BaseVariable<string>
    {
    } 
}