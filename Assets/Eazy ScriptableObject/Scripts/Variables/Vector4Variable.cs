using UnityEngine;

namespace EazyScriptableObject
{
    [CreateAssetMenu(
        fileName = "Vector4Variable.asset",
        menuName = EazySOUtility.ADD_COMPONENT_MENU_ROOT + EazySOUtility.VARIABLE_SUBMENU + "Structs/Vector4",
        order = EazySOUtility.ASSET_MENU_ORDER_VARIABLES)]
    public sealed class Vector4Variable : BaseVariable<Vector4>
    {
    } 
}