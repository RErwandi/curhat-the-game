﻿using UnityEngine;

namespace EazyScriptableObject
{
    [CreateAssetMenu(
        fileName = "ObjectVariable.asset",
        menuName = EazySOUtility.ADD_COMPONENT_MENU_ROOT + EazySOUtility.VARIABLE_SUBMENU + "Object",
        order = EazySOUtility.ASSET_MENU_ORDER_VARIABLES)]
    public class ObjectVariable : BaseVariable<Object>
    {
    } 
}