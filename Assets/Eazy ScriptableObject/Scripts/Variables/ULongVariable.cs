﻿using UnityEngine;

namespace EazyScriptableObject
{
    [CreateAssetMenu(
        fileName = "UnsignedLongVariable.asset",
        menuName = EazySOUtility.ADD_COMPONENT_MENU_ROOT + EazySOUtility.VARIABLE_SUBMENU + "ulong",
        order = EazySOUtility.ASSET_MENU_ORDER_VARIABLES)]
    public class ULongVariable : BaseVariable<ulong>
    {
    } 
}