using UnityEngine;

namespace EazyScriptableObject
{
	[CreateAssetMenu(
	    fileName = "AudioClipVariable.asset",
	    menuName = EazySOUtility.ADD_COMPONENT_MENU_ROOT + EazySOUtility.VARIABLE_SUBMENU + "AudioClip",
	    order = EazySOUtility.ASSET_MENU_ORDER_VARIABLES)]
	public class AudioClipVariable : BaseVariable<AudioClip>
	{
	}
}