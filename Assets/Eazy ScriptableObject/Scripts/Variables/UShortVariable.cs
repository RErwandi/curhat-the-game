﻿using UnityEngine;

namespace EazyScriptableObject
{
    [CreateAssetMenu(
        fileName = "UnsignedShortVariable.asset",
        menuName = EazySOUtility.ADD_COMPONENT_MENU_ROOT + EazySOUtility.VARIABLE_SUBMENU +"ushort",
        order = EazySOUtility.ASSET_MENU_ORDER_VARIABLES)]
    public class UShortVariable : BaseVariable<ushort>
    {
    } 
}