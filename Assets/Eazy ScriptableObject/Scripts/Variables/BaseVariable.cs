﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace EazyScriptableObject
{
    public abstract class BaseVariable<T> : GameEventBase<T>
    {

        // Current value
        [HideInEditorMode]
        [OnValueChanged("RaiseT")]
        [PropertyOrder(-1)]
        [SerializeField] private T value;
        
        // Initial value
        [HideInPlayMode]
        [OnValueChanged("UpdateValue")]
        [PropertyOrder(-2)]
        [SerializeField] private T initValue = default;

        public T Value
        {
            get => value;
            set
            {
                this.value = value;
                Raise(value);
            }
        }
        
        public override void AddListener(Action<T> listener)
        {
            base.AddListener(listener);
            listener.Invoke(value);
#if UNITY_EDITOR
            Application.quitting += Reset;
#endif
        }
        
        public override void AddListener(Action listener)
        {
            base.AddListener(listener);
            listener.Invoke();
#if UNITY_EDITOR
            Application.quitting += Reset;
#endif
        }

        public override void RemoveListener(Action<T> listener)
        {
            base.RemoveListener(listener);
#if UNITY_EDITOR
            Application.quitting -= Reset;
#endif
        }

        public override void RemoveListener(Action listener)
        {
            base.RemoveListener(listener);
#if UNITY_EDITOR
            Application.quitting -= Reset;
#endif
        }

        private void RaiseT()
        {
            Raise(value);
        }

        private void UpdateValue()
        {
            value = initValue;
        }

        public override string ToString()
        {
            return Value == null ? "null" : Value.ToString();
        }
        
        protected override bool ShowDebuggingOptions()
        {
            return false;
        }
        
        public static implicit operator T(BaseVariable<T> variable)
        {
            return variable.Value;
        }
        
        public void Reset()
        {
            value = initValue;
        }
    } 
}