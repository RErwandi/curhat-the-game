using UnityEngine;

namespace EazyScriptableObject
{
    [CreateAssetMenu(
        fileName = "SByteCollection.asset",
        menuName = EazySOUtility.ADD_COMPONENT_MENU_ROOT + EazySOUtility.COLLECTION_SUBMENU + "sbyte",
        order = EazySOUtility.ASSET_MENU_ORDER_COLLECTIONS)]
    public class SByteCollection : Collection<sbyte>
    {
    } 
}