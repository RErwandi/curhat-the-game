using UnityEngine;

namespace EazyScriptableObject
{
    [CreateAssetMenu(
        fileName = "ShortCollection.asset",
        menuName = EazySOUtility.ADD_COMPONENT_MENU_ROOT + EazySOUtility.COLLECTION_SUBMENU + "short",
        order = EazySOUtility.ASSET_MENU_ORDER_COLLECTIONS)]
    public class ShortCollection : Collection<short>
    {
    } 
}