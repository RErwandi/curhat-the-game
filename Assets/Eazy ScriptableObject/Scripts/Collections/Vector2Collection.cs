using UnityEngine;

namespace EazyScriptableObject
{
    [CreateAssetMenu(
        fileName = "Vector2Collection.asset",
        menuName = EazySOUtility.ADD_COMPONENT_MENU_ROOT + EazySOUtility.COLLECTION_SUBMENU + "Structs/Vector2",
        order = EazySOUtility.ASSET_MENU_ORDER_COLLECTIONS)]
    public class Vector2Collection : Collection<Vector2>
    {
    } 
}