using UnityEngine;

namespace EazyScriptableObject
{
	[CreateAssetMenu(
	    fileName = "Color32Collection.asset",
	    menuName = EazySOUtility.ADD_COMPONENT_MENU_ROOT + EazySOUtility.COLLECTION_SUBMENU + "Structs/Color32",
	    order = EazySOUtility.ASSET_MENU_ORDER_COLLECTIONS)]
	public class Color32Collection : Collection<Color32>
	{
	}
}