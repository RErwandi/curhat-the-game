using UnityEngine;

namespace EazyScriptableObject
{
    [CreateAssetMenu(
        fileName = "QuaternionCollection.asset",
        menuName = EazySOUtility.ADD_COMPONENT_MENU_ROOT + EazySOUtility.COLLECTION_SUBMENU + "Structs/Quaternion",
        order = EazySOUtility.ASSET_MENU_ORDER_COLLECTIONS)]
    public class QuaternionCollection : Collection<Quaternion>
    {
    } 
}