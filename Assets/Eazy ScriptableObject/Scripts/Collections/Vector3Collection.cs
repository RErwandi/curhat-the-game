using UnityEngine;

namespace EazyScriptableObject
{
    [CreateAssetMenu(
        fileName = "Vector3Collection.asset",
        menuName = EazySOUtility.ADD_COMPONENT_MENU_ROOT + EazySOUtility.COLLECTION_SUBMENU + "Structs/Vector3",
        order = EazySOUtility.ASSET_MENU_ORDER_COLLECTIONS)]
    public class Vector3Collection : Collection<Vector3>
    {
    } 
}