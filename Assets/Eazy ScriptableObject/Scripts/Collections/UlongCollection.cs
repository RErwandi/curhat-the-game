using UnityEngine;

namespace EazyScriptableObject
{
    [CreateAssetMenu(
        fileName = "ULongCollection.asset",
        menuName = EazySOUtility.ADD_COMPONENT_MENU_ROOT + EazySOUtility.COLLECTION_SUBMENU + "ulong",
        order = EazySOUtility.ASSET_MENU_ORDER_COLLECTIONS)]
    public class ULongCollection : Collection<ulong>
    {
    } 
}