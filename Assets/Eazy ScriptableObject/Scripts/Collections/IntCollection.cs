using UnityEngine;

namespace EazyScriptableObject
{
    [CreateAssetMenu(
        fileName = "IntCollection.asset",
        menuName = EazySOUtility.ADD_COMPONENT_MENU_ROOT + EazySOUtility.COLLECTION_SUBMENU + "int",
        order = EazySOUtility.ASSET_MENU_ORDER_COLLECTIONS)]
    public class IntCollection : Collection<int>
    {
    } 
}