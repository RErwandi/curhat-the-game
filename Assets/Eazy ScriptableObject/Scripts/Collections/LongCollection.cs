using UnityEngine;

namespace EazyScriptableObject
{
    [CreateAssetMenu(
        fileName = "LongCollection.asset",
        menuName = EazySOUtility.ADD_COMPONENT_MENU_ROOT + EazySOUtility.COLLECTION_SUBMENU + "long",
        order = EazySOUtility.ASSET_MENU_ORDER_COLLECTIONS)]
    public class LongCollection : Collection<long>
    {
    } 
}