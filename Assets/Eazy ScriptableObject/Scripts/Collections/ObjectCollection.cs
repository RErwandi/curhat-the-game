﻿using UnityEngine;

namespace EazyScriptableObject
{
    [CreateAssetMenu(
        fileName = "ObjectCollection.asset",
        menuName = EazySOUtility.ADD_COMPONENT_MENU_ROOT + EazySOUtility.COLLECTION_SUBMENU + "Object",
        order = EazySOUtility.ASSET_MENU_ORDER_COLLECTIONS)]
    public class ObjectCollection : Collection<Object>
    {
    } 
}