﻿using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Serialization;

namespace EazyScriptableObject
{
    public class Collection<T> : BaseCollection, IEnumerable<T>, ISerializationCallbackReceiver
    {
        public new T this[int index]
        {
            get
            {
                return list[index];
            }
            set
            {
                list[index] = value;
            }
        }

        [OnValueChanged("CopyList")]
        [DisableInPlayMode]
        [SerializeField] private List<T> startingList = new List<T>();
        [DisableInEditorMode]
        [SerializeField] private List<T> list = new List<T>();
        [SerializeField] private GameEvent onCollectionChanged = default;

        public override IList List => list;

        public override Type Type => typeof(T);

        public void Add(T obj)
        {
            if (!list.Contains(obj))
            {
                list.Add(obj);
                onCollectionChanged.Raise();
            }
        }
        
        public void Remove(T obj)
        {
            if (list.Contains(obj))
            {
                list.Remove(obj);
                onCollectionChanged.Raise();
            }
        }
        
        public void Clear()
        {
            list.Clear();
            onCollectionChanged.Raise();
        }
        
        public bool Contains(T value)
        {
            return list.Contains(value);
        }
        
        public int IndexOf(T value)
        {
            return list.IndexOf(value);
        }
        
        public void RemoveAt(int index)
        {
            list.RemoveAt(index);
        }
        
        public void Insert(int index, T value)
        {
            list.Insert(index, value);
        }

        private void CopyList()
        {
            list = new List<T>(startingList);
        }
        
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
        
        public IEnumerator<T> GetEnumerator()
        {
            return list.GetEnumerator();
        }
        
        public override string ToString()
        {
            return "Collection<" + typeof(T) + ">(" + Count + ")";
        }

        public void OnBeforeSerialize()
        {
            
        }

        public void OnAfterDeserialize()
        {
            CopyList();
        }
    } 
}