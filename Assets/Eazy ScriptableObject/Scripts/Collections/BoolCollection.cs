using UnityEngine;

namespace EazyScriptableObject
{
    [CreateAssetMenu(
        fileName = "BoolCollection.asset",
        menuName = EazySOUtility.ADD_COMPONENT_MENU_ROOT + EazySOUtility.COLLECTION_SUBMENU + "bool",
        order = EazySOUtility.ASSET_MENU_ORDER_COLLECTIONS)]
    public class BoolCollection : Collection<bool>
    {
    } 
}