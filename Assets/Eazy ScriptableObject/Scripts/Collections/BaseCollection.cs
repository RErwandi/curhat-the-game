﻿using System.Collections;
using UnityEngine;
using Type = System.Type;

namespace EazyScriptableObject
{
    public abstract class BaseCollection : UnityEngine.ScriptableObject, IEnumerable
    {
        public object this[int index]
        {
            get
            {
                return List[index];
            }
            set
            {
                List[index] = value;
            }
        }

        public abstract IList List { get; }
        public abstract Type Type { get; }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return List.GetEnumerator();
        }
        public bool Contains(object obj)
        {
            return List.Contains(obj);
        }
        public int Count { get { return List.Count; } }
	}
}