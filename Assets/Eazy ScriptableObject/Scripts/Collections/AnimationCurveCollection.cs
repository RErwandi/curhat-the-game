using UnityEngine;

namespace EazyScriptableObject
{
	[CreateAssetMenu(
	    fileName = "AnimationCurveCollection.asset",
	    menuName = EazySOUtility.ADD_COMPONENT_MENU_ROOT + EazySOUtility.COLLECTION_SUBMENU + "AnimationCurve",
	    order = EazySOUtility.ASSET_MENU_ORDER_COLLECTIONS)]
	public class AnimationCurveCollection : Collection<AnimationCurve>
	{
	}
}