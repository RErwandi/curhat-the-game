using UnityEngine;

namespace EazyScriptableObject
{
    [CreateAssetMenu(
        fileName = "Vector4Collection.asset",
        menuName = EazySOUtility.ADD_COMPONENT_MENU_ROOT + EazySOUtility.COLLECTION_SUBMENU + "Structs/Vector4",
        order = EazySOUtility.ASSET_MENU_ORDER_COLLECTIONS)]
    public class Vector4Collection : Collection<Vector4>
    {
    } 
}