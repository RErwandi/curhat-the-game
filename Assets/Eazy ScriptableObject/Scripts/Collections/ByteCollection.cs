using UnityEngine;

namespace EazyScriptableObject
{
    [CreateAssetMenu(
        fileName = "ByteCollection.asset",
        menuName = EazySOUtility.ADD_COMPONENT_MENU_ROOT + EazySOUtility.COLLECTION_SUBMENU + "byte",
        order = EazySOUtility.ASSET_MENU_ORDER_COLLECTIONS)]
    public class ByteCollection : Collection<byte>
    {
    } 
}