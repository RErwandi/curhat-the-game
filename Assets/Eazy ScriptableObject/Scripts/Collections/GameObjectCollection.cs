using UnityEngine;

namespace EazyScriptableObject
{
    [CreateAssetMenu(
        fileName = "GameObjectCollection.asset",
        menuName = EazySOUtility.ADD_COMPONENT_MENU_ROOT + EazySOUtility.COLLECTION_SUBMENU + "GameObject",
        order = EazySOUtility.ASSET_MENU_ORDER_COLLECTIONS)]
    public class GameObjectCollection : Collection<GameObject>
    {
    } 
}
