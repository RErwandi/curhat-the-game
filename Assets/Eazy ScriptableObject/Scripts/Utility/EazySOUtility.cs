﻿namespace EazyScriptableObject
{
    public static class EazySOUtility
    {
        public const string ADD_COMPONENT_MENU_ROOT = "Eazy ScriptableObject/";
        public const string VARIABLE_SUBMENU = "Variables/";
        public const string EVENT_LISTENER_SUBMENU = "Event Listeners/";
        public const string GAME_EVENT_SUBMENU = "Game Events/";
        public const string COLLECTION_SUBMENU = "Collections/";
        public const string INSPECTOR_SUBMENU = "Scene Inspector/";

        public const int ASSET_MENU_ORDER_VARIABLES = 150;
        public const int ASSET_MENU_ORDER_EVENTS = 50;
        public const int ASSET_MENU_ORDER_COLLECTIONS = 250;
        public const int ASSET_MENU_ORDER_INSPECTOR = 350;
        public const int ASSET_MENU_ORDER_CUSTOM = 25;
    }
}