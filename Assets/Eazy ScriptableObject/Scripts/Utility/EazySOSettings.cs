﻿using Sirenix.OdinInspector;
using Sirenix.Utilities;
using UnityEngine;

namespace EazyScriptableObject
{
    [GlobalConfig("Assets/Eazy ScriptableObject/Resources/")]
    public class EazySOSettings : GlobalConfig<EazySOSettings>
    {
        [FolderPath]
        public string scriptableObjectDatabase;

        [FolderPath]
        public string sceneInspectorDatabase;

        [FolderPath]
        public string codeGenerationDirectory;
        public bool codeGenerationAllowOverwrite = true;
        public bool showDebugWhenEventRaised = false;
    }
}