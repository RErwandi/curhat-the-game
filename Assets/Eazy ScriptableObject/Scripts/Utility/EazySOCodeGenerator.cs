﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

namespace EazyScriptableObject
{
    public static class EazySOCodeGenerator
    {
        private const int TYPE_COUNT = 8;
        
        public struct Data
        {
            public string TypeName;
            public string NameSpace;
            public string FieldString;
        }

        private static readonly string[] TemplateNames = new string[TYPE_COUNT]
        {
            "ClassTemplate",
            "GameEventListenerTemplate",
            "GameEventTemplate",
            "InspectorTemplate",
            "ReferenceTemplate",
            "CollectionTemplate",
            "UnityEventTemplate",
            "VariableTemplate",
        };

        private static readonly string[] TargetFileNames = new string[TYPE_COUNT]
        {
            "{0}.cs",
            "{0}GameEventListener.cs",
            "{0}GameEvent.cs",
            "{0}GameEventInspector.cs",
            "{0}Reference.cs",
            "{0}Collection.cs",
            "{0}UnityEvent.cs",
            "{0}Variable.cs",
        };
        
        private static string[] targetDirectories = null;
        private static string[,] replacementStrings = null;
        private static readonly string[] TemplatePaths = new string[TYPE_COUNT];
        
        private static string TypeName => replacementStrings[1, 1];

        static EazySOCodeGenerator()
        {
            CreateTargetDirectories();
            GatherFilePaths();
        }
        
        private static void CreateTargetDirectories()
        {
            targetDirectories = new string[TYPE_COUNT]
            {
                EazySOSettings.Instance.codeGenerationDirectory + "/Classes",
                EazySOSettings.Instance.codeGenerationDirectory + "/Events/Listeners",
                EazySOSettings.Instance.codeGenerationDirectory + "/Events/Game Events",
                EazySOSettings.Instance.codeGenerationDirectory + "/Inspector",
                EazySOSettings.Instance.codeGenerationDirectory + "/References",
                EazySOSettings.Instance.codeGenerationDirectory + "/Collections",
                EazySOSettings.Instance.codeGenerationDirectory + "/Events/Responses",
                EazySOSettings.Instance.codeGenerationDirectory + "/Variables",
            };
        }

        private static void GatherFilePaths()
        {
            string assetPath = Application.dataPath;
            string folderToStartSearch = Directory.GetParent(assetPath).FullName;

            Queue<string> foldersToCheck = new Queue<string>();
            foldersToCheck.Enqueue(folderToStartSearch);

            while (foldersToCheck.Count > 0)
            {
                string currentDirectory = foldersToCheck.Dequeue();

                foreach (string filePath in Directory.GetFiles(currentDirectory))
                {
                    string fileName = Path.GetFileName(filePath);

                    for (int i = 0; i < TYPE_COUNT; i++)
                    {
                        if (TemplateNames[i] == fileName)
                            TemplatePaths[i] = filePath;
                    }
                }

                foreach (string subDirectory in Directory.GetDirectories(currentDirectory))
                {
                    foldersToCheck.Enqueue(subDirectory);
                }
            }
        }
        
        public static void Generate(Data data)
        {
            replacementStrings = new string[4, 2]
            {
                { "$TYPE$", data.TypeName },
                { "$TYPE_NAME$", CapitalizeFirstLetter(data.TypeName) },
                { "$NAMESPACE$", data.NameSpace },
                { "$FIELDS$", data.FieldString}
            };

            for (int i = 0; i < TYPE_COUNT; i++)
            {
                GenerateScript(i);
            }
        }
        
        private static void GenerateScript(int index)
        {
            string targetFilePath = GetTargetFilePath(index);
            string contents = GetScriptContents(index);

            if (File.Exists(targetFilePath) && !EazySOSettings.Instance.codeGenerationAllowOverwrite)
            {
                Debug.Log("Cannot create file at " + targetFilePath + " because a file already exists, and overwrites are disabled");
                return;
            }

            Debug.Log("Creating " + targetFilePath);

            Directory.CreateDirectory(Path.GetDirectoryName(targetFilePath));
            File.WriteAllText(targetFilePath, contents);
        }
        
        private static string GetScriptContents(int index)
        {
            string templatePath = TemplatePaths[index];
            string templateContent = File.ReadAllText(templatePath);

            string output = templateContent;

            for (int i = 0; i < replacementStrings.GetLength(0); i++)
            {
                output = output.Replace(replacementStrings[i, 0], replacementStrings[i, 1]);
            }

            return output;
        }
        private static string GetTargetFilePath(int index)
        {
            return targetDirectories[index] + "/" + string.Format(TargetFileNames[index], TypeName);
        }
        
        private static string CapitalizeFirstLetter(string input)
        {
            return input.First().ToString().ToUpper() + input.Substring(1);
        }
    }
}