﻿using UnityEngine;

namespace EazyScriptableObject
{
    [CreateAssetMenu(fileName = "Vector3 GameEvent Inspector.asset",
        menuName = EazySOUtility.ADD_COMPONENT_MENU_ROOT + EazySOUtility.INSPECTOR_SUBMENU + "vector3",
        order = EazySOUtility.ASSET_MENU_ORDER_INSPECTOR)]
    public class Vector3GameEventInspector : ScriptableObjectSceneInspector<Vector3, Vector3GameEvent, Vector3UnityEvent>
    {
        
    }
}