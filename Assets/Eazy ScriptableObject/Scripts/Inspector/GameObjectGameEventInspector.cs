﻿using UnityEngine;

namespace EazyScriptableObject
{
    [CreateAssetMenu(fileName = "GameObject GameEvent Inspector.asset",
        menuName = EazySOUtility.ADD_COMPONENT_MENU_ROOT + EazySOUtility.INSPECTOR_SUBMENU + "GameObject",
        order = EazySOUtility.ASSET_MENU_ORDER_INSPECTOR)]
    public class GameObjectGameEventInspector : ScriptableObjectSceneInspector<GameObject, GameObjectGameEvent, GameObjectUnityEvent>
    {
        
    }
}