﻿using UnityEngine;

namespace EazyScriptableObject
{
    [CreateAssetMenu(fileName = "Float GameEvent Inspector.asset",
        menuName = EazySOUtility.ADD_COMPONENT_MENU_ROOT + EazySOUtility.INSPECTOR_SUBMENU + "float",
        order = EazySOUtility.ASSET_MENU_ORDER_INSPECTOR)]
    public class FloatGameEventInspector : ScriptableObjectSceneInspector<float, FloatGameEvent, FloatUnityEvent>
    {
        
    }
}