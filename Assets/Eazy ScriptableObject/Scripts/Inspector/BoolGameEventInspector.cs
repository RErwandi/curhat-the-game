﻿using UnityEngine;

namespace EazyScriptableObject
{
    [CreateAssetMenu(fileName = "Bool GameEvent Inspector.asset",
        menuName = EazySOUtility.ADD_COMPONENT_MENU_ROOT + EazySOUtility.INSPECTOR_SUBMENU + "bool",
        order = EazySOUtility.ASSET_MENU_ORDER_INSPECTOR)]
    public class BoolGameEventInspector : ScriptableObjectSceneInspector<bool, BoolGameEvent, BoolUnityEvent>
    {
        
    }
}