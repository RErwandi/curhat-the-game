﻿using UnityEngine;

namespace EazyScriptableObject
{
    [CreateAssetMenu(fileName = "String GameEvent Inspector.asset",
        menuName = EazySOUtility.ADD_COMPONENT_MENU_ROOT + EazySOUtility.INSPECTOR_SUBMENU + "string",
        order = EazySOUtility.ASSET_MENU_ORDER_INSPECTOR)]
    public class StringGameEventInspector : ScriptableObjectSceneInspector<string, StringGameEvent, StringUnityEvent>
    {
        
    }
}