﻿using UnityEngine;

namespace EazyScriptableObject
{
    [CreateAssetMenu(fileName = "Byte GameEvent Inspector.asset",
        menuName = EazySOUtility.ADD_COMPONENT_MENU_ROOT + EazySOUtility.INSPECTOR_SUBMENU + "byte",
        order = EazySOUtility.ASSET_MENU_ORDER_INSPECTOR)]
    public class ByteGameEventInspector : ScriptableObjectSceneInspector<byte, ByteGameEvent, ByteUnityEvent>
    {
        
    }
}