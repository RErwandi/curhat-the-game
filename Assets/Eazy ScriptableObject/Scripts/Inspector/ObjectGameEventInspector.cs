﻿using UnityEngine;

namespace EazyScriptableObject
{
    [CreateAssetMenu(fileName = "Object GameEvent Inspector.asset",
        menuName = EazySOUtility.ADD_COMPONENT_MENU_ROOT + EazySOUtility.INSPECTOR_SUBMENU + "Object",
        order = EazySOUtility.ASSET_MENU_ORDER_INSPECTOR)]
    public class ObjectGameEventInspector : ScriptableObjectSceneInspector<Object, ObjectGameEvent, ObjectUnityEvent>
    {
        
    }
}