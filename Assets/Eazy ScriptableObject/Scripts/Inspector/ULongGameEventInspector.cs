﻿using UnityEngine;

namespace EazyScriptableObject
{
    [CreateAssetMenu(fileName = "ULong GameEvent Inspector.asset",
        menuName = EazySOUtility.ADD_COMPONENT_MENU_ROOT + EazySOUtility.INSPECTOR_SUBMENU + "ulong",
        order = EazySOUtility.ASSET_MENU_ORDER_INSPECTOR)]
    public class ULongGameEventInspector : ScriptableObjectSceneInspector<ulong, ULongGameEvent, ULongUnityEvent>
    {
        
    }
}