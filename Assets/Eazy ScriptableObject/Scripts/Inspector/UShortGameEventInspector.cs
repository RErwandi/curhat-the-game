﻿using UnityEngine;

namespace EazyScriptableObject
{
    [CreateAssetMenu(fileName = "UShort GameEvent Inspector.asset",
        menuName = EazySOUtility.ADD_COMPONENT_MENU_ROOT + EazySOUtility.INSPECTOR_SUBMENU + "ushort",
        order = EazySOUtility.ASSET_MENU_ORDER_INSPECTOR)]
    public class UShortGameEventInspector : ScriptableObjectSceneInspector<ushort, UShortGameEvent, UShortUnityEvent>
    {
        
    }
}