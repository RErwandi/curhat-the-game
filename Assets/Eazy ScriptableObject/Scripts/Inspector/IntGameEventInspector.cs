﻿using UnityEngine;

namespace EazyScriptableObject
{
    [CreateAssetMenu(fileName = "Int GameEvent Inspector.asset",
        menuName = EazySOUtility.ADD_COMPONENT_MENU_ROOT + EazySOUtility.INSPECTOR_SUBMENU + "int",
        order = EazySOUtility.ASSET_MENU_ORDER_INSPECTOR)]
    public class IntGameEventInspector : ScriptableObjectSceneInspector<int, IntGameEvent, IntUnityEvent>
    {
        
    }
}