﻿using UnityEngine;

namespace EazyScriptableObject
{
    [CreateAssetMenu(fileName = "Char GameEvent Inspector.asset",
        menuName = EazySOUtility.ADD_COMPONENT_MENU_ROOT + EazySOUtility.INSPECTOR_SUBMENU + "char",
        order = EazySOUtility.ASSET_MENU_ORDER_INSPECTOR)]
    public class CharGameEventInspector : ScriptableObjectSceneInspector<char, CharGameEvent, CharUnityEvent>
    {
        
    }
}