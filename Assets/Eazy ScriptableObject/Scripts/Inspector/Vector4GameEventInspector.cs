﻿using UnityEngine;

namespace EazyScriptableObject
{
    [CreateAssetMenu(fileName = "Vector4 GameEvent Inspector.asset",
        menuName = EazySOUtility.ADD_COMPONENT_MENU_ROOT + EazySOUtility.INSPECTOR_SUBMENU + "vector4",
        order = EazySOUtility.ASSET_MENU_ORDER_INSPECTOR)]
    public class Vector4GameEventInspector : ScriptableObjectSceneInspector<Vector4, Vector4GameEvent, Vector4UnityEvent>
    {
        
    }
}