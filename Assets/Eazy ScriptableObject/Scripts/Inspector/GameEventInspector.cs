﻿using UnityEngine;
using UnityEngine.Events;

namespace EazyScriptableObject
{
    [CreateAssetMenu(fileName = "GameEvent Inspector.asset",
        menuName = EazySOUtility.ADD_COMPONENT_MENU_ROOT + EazySOUtility.INSPECTOR_SUBMENU + "Game Event",
        order = EazySOUtility.ASSET_MENU_ORDER_INSPECTOR)]
    public class GameEventInspector : ScriptableObjectSceneInspector<GameEventBase, UnityEvent>
    {
        
    }
}