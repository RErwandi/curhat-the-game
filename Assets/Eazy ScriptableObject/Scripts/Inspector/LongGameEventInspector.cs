﻿using UnityEngine;

namespace EazyScriptableObject
{
    [CreateAssetMenu(fileName = "Long GameEvent Inspector.asset",
        menuName = EazySOUtility.ADD_COMPONENT_MENU_ROOT + EazySOUtility.INSPECTOR_SUBMENU + "long",
        order = EazySOUtility.ASSET_MENU_ORDER_INSPECTOR)]
    public class LongGameEventInspector : ScriptableObjectSceneInspector<long, LongGameEvent, LongUnityEvent>
    {
        
    }
}