﻿using UnityEngine;

namespace EazyScriptableObject
{
    [CreateAssetMenu(fileName = "Vector2 GameEvent Inspector.asset",
        menuName = EazySOUtility.ADD_COMPONENT_MENU_ROOT + EazySOUtility.INSPECTOR_SUBMENU + "vector2",
        order = EazySOUtility.ASSET_MENU_ORDER_INSPECTOR)]
    public class Vector2GameEventInspector : ScriptableObjectSceneInspector<Vector2, Vector2GameEvent, Vector2UnityEvent>
    {
        
    }
}