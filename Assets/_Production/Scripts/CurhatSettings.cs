﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.Utilities;
using UnityEngine;

namespace Reynold.Curhat
{
    [GlobalConfig("_Production/Resources/Config")]
    public class CurhatSettings : GlobalConfig<CurhatSettings>
    {
        public string hostUrl;
        public string postCurhatUrl;
        public string postReplyUrl;
        public string postMessageUrl;
        public string postReadCurhatUrl;
        public string postReadMessageUrl;
        public string getCurhatUrl;
        public string getMyCurhatUrl;
        public string getReplyUrl;
        public string getMessageUrl;
    }
}