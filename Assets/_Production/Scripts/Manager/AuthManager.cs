﻿using System;
using System.Collections;
using System.Collections.Generic;
using Doozy.Engine;
using Doozy.Engine.UI;
using EazyScriptableObject;
using Microsoft.Applications.Events.DataModels;
using PlayFab;
using PlayFab.ClientModels;
using UnityEngine;

namespace Reynold.Curhat
{
    public class AuthManager : Singleton<AuthManager>
    {
        public string popupName;
        public string loadingPopup;
        public StringVariable playerId;
        public StringVariable playerDisplayName;
        public StringVariable playerInitial;
        public StringVariable playerAvatar;
        public GameEvent onLoginSuccess;
        public GameEvent onFirstTimeLogin;

        private string androidId;
        
        void Start()
        {
            GetDeviceId();
        }

        void GetDeviceId()
        {
            if (Application.platform == RuntimePlatform.Android)
            {
                AndroidJavaClass clsUnity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
                AndroidJavaObject objActivity = clsUnity.GetStatic<AndroidJavaObject>("currentActivity");
                AndroidJavaObject objResolver = objActivity.Call<AndroidJavaObject>("getContentResolver");
                AndroidJavaClass clsSecure = new AndroidJavaClass("android.provider.Settings$Secure");
                androidId = clsSecure.CallStatic<string>("getString", objResolver, "android_id");
            }
        }

        public void TryLoginAsGuest()
        {
            // TO DO : If this is the first time user login with guest, show guest login popup
            ShowPopup();
        }

        private void ShowPopup()
        {
            var popup = UIPopup.GetPopup(popupName);
            popup.Data.SetButtonsCallbacks(HidePopup, LoginWithDeviceId);
            popup.Show();
        }
        
        private void LoginWithDeviceId()
        {
            switch (Application.platform)
            {
               case RuntimePlatform.Android:
                   var androidRequest = new LoginWithAndroidDeviceIDRequest{AndroidDeviceId = androidId, CreateAccount = true};
                   PlayFabClientAPI.LoginWithAndroidDeviceID(androidRequest, OnLoginSuccess, OnLoginFailure);
                   break;
               default:
                   var request = new LoginWithCustomIDRequest {CustomId = "Unity Editor", CreateAccount = true};
                   PlayFabClientAPI.LoginWithCustomID(request, OnLoginSuccess, OnLoginFailure);
                   break;
            }

            HidePopup();
            ShowLoadingPopup();
        }
        
        private void HidePopup()
        {
            var popup = UIPopup.LastShownPopup;
            popup.Hide();
        }

        private void ShowLoadingPopup()
        {
            var popup = UIPopup.GetPopup(loadingPopup);
            popup.Show();
        }

        private void OnLoginSuccess(LoginResult result)
        {
            Debug.Log("Login Success");
            GetPlayerData();
        }

        private void OnLoginFailure(PlayFabError error)
        {
            Debug.Log("Login Failed");
        }
        
        private void GetPlayerData()
        {
            var request = new GetAccountInfoRequest();
            PlayFabClientAPI.GetAccountInfo(request, OnGetAccountInfoSuccess, OnGetAccountInfoFailure);
        }

        private void OnGetAccountInfoSuccess(GetAccountInfoResult obj)
        {
            playerId.Value = obj.AccountInfo.PlayFabId;
            playerDisplayName.Value = obj.AccountInfo.Username;
            Debug.Log($"Get account ID : {obj.AccountInfo.PlayFabId}");
            Debug.Log($"Get account username : {obj.AccountInfo.Username}");
            if (String.IsNullOrEmpty(playerDisplayName.Value))
            {
                onFirstTimeLogin.Raise();
            }
            else
            {
                playerInitial.Value = playerDisplayName.Value;
                onLoginSuccess.Raise();
            }
            
            HidePopup();
        }
        
        private void OnGetAccountInfoFailure(PlayFabError obj)
        {
            Debug.Log("Get account info failure : " + obj.Error);
        }

        public void RegisterUserDisplayName()
        {
            ShowLoadingPopup();
            
            var updateDisplayNameRequest = new UpdateUserTitleDisplayNameRequest();
            updateDisplayNameRequest.DisplayName = playerDisplayName.Value;

            PlayFabClientAPI.UpdateUserTitleDisplayName(updateDisplayNameRequest,
                result => RegisterUserAvatar(), error => Debug.Log(error.Error));
            
            
        }

        private void RegisterUserAvatar()
        {
            var request = new UpdateUserDataRequest();
            request.Data = new Dictionary<string, string>()
            {
                {"Avatar", playerAvatar.Value}
            };
            
            PlayFabClientAPI.UpdateUserData(request, result =>
            {
                onLoginSuccess.Raise();
                HidePopup();
            }, error => Debug.Log(error.Error));
        }
    }
}