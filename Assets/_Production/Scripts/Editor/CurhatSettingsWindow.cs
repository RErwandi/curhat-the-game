﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using Sirenix.OdinInspector.Editor;
using UnityEditor;
using UnityEngine;

namespace Reynold.Curhat
{
    public class CurhatSettingsWindow : OdinEditorWindow
    {
        [MenuItem("Reynold/Curhat Settings")]
        private static void OpenWindow()
        {
            GetWindow<CurhatSettingsWindow>().Show();
        }

        [InlineEditor(InlineEditorObjectFieldModes.CompletelyHidden)]
        public CurhatSettings settings;

        protected override void OnEnable()
        {
            base.OnEnable();
            settings = CurhatSettings.Instance;
        }
    }
}