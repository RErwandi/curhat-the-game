﻿using System;
using System.Collections;
using System.Collections.Generic;
using Doozy.Engine.UI;
using UnityEngine;
using UnityEngine.UI;

namespace Reynold.Curhat
{
    public class LoginView : MonoBehaviour
    {
        public UIButton facebookButton;
        public UIButton googleButton;
        public UIButton guestButton;

        private void OnEnable()
        {
            guestButton.OnClick.OnTrigger.Event.AddListener(AuthManager.Instance.TryLoginAsGuest);
        }

        private void OnDisable()
        {
            guestButton.OnClick.OnTrigger.Event.RemoveListener(AuthManager.Instance.TryLoginAsGuest);
        }

        void Start()
        {
        
        }
        
        void Update()
        {
        
        }
    }
}