﻿using System;
using System.Collections;
using System.Collections.Generic;
using EazyScriptableObject;
using Reynold.UI;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Reynold.Curhat
{
    public class FirstInfoView : MonoBehaviour
    {
        public MultiSelect data;
        public TMP_InputField nameInput;
        public StringVariable playerDisplayName;
        public StringVariable playerAvatar;
        public Button confirmButton;

        private void Start()
        {
            data.SetButtonsCallbacks(ChooseMale, ChooseFemale);
            nameInput.onValueChanged.AddListener(UpdateDisplayName);
            confirmButton.onClick.AddListener(AuthManager.Instance.RegisterUserDisplayName);
        }

        private void UpdateDisplayName(string arg0)
        {
            playerDisplayName.Value = arg0;
            confirmButton.interactable = arg0 != "";
        }

        private void ChooseMale()
        {
            Debug.Log("Choosing Male");
            playerAvatar.Value = "Avatar1";
        }

        private void ChooseFemale()
        {
            Debug.Log("Choosing Female");
            playerAvatar.Value = "Avatar2";
        }
    }
}