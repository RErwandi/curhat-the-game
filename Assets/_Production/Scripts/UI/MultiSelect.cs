﻿using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Reynold.UI
{
    public abstract class MultiSelect : MonoBehaviour
    {
        public List<MultiSelectItem> items;
        public bool useDefault = false;
        [ShowIf("useDefault")]
        public int defaultIndex = 0;

        private void Start()
        {
            InitItems();
            Reset();
        }

        private void InitItems()
        {
            for (int i = 0; i < items.Count; i++)
            {
                var item = items[i];
                item.button.onClick.AddListener(() => Switch(item));
            }
        }

        public void SetButtonsCallbacks(params UnityAction[] callbacks)
        {
            if (callbacks == null || callbacks.Length == 0) return;
            for (int i = 0; i < items.Count; i++)
            {
                var item = items[i];
                if (item == null) continue;
                if (callbacks[i] == null) continue;
                item.button.onClick.AddListener(callbacks[i]);
            }
        }

        private void Switch(MultiSelectItem item)
        {
            for (int i = 0; i < items.Count; i++)
            {
                if (items[i] == item)
                {
                    items[i].Select();
                }
                else
                {
                    items[i].Deselect();
                }
            }
        }

        private void Reset()
        {
            if (useDefault)
            {
                Switch(items[defaultIndex]);
            }
            else
            {
                Switch(null);
            }
        }
    }
}