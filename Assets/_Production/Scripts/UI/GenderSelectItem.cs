﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Reynold.UI
{
    public class GenderSelectItem : MultiSelectItem
    {
        public Image portrait;

        public override void Select()
        {
            portrait.color = Color.white;
        }

        public override void Deselect()
        {
            portrait.color = Color.gray;
        }
    }
}