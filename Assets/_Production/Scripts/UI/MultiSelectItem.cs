﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Reynold.UI
{
    public abstract class MultiSelectItem : MonoBehaviour
    {
        public Button button;
        public abstract void Select();
        public abstract void Deselect();
    }
}